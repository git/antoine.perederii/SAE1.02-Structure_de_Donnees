/**
 * @file sauvegarde.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant les fonctions de sauvegarde
 *
 */

#include "SAE.h"

/**
 * @brief Sauvegarde le tableau des IUT dans le fichier des IUT
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nb Nombre d'IUT
 */
void sauvegardeVillesIUT(VilleIUT *tiut[], int nb)
{
    char ville[30], dept[30], resp[30];
    int nbP;
    FILE *file = fopen("informationsIUT.txt", "w");
    if (file == NULL)
    {
        printf("Fonction sauvegarde : Problème lors de l'ouverture du fichier informationsIUT.txt\n");
        exit(-1);
    }
    for (int i = 0; i < nb; i++)
    {
        strcpy(ville, tiut[i]->ville);
        while (!vide(tiut[i]->ldept))
        {
            strcpy(dept, getDept(tiut[i]->ldept, 0));
            nbP = getNbP(tiut[i]->ldept, 0);
            strcpy(resp, getResp(tiut[i]->ldept, 0));
            fprintf(file, "%s %s %d %s\n", ville, dept, nbP, resp);
            tiut[i]->ldept = supprimerEnTete(tiut[i]->ldept);
        }
    }
    fclose(file);
}

/**
 * @brief Sauvegarde les tableaux des candidats dans leurs fichiers respectifs
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param nomFichier Nom du fichier dans lequel sauvegarder
 */
void sauvegarderCandidats(Etudiant *tetud[], int nbCandidats, char nomFichier[])
{
    FILE *file;
    file = fopen(nomFichier, "w");
    if (file == NULL)
    {
        printf("Erreur d'ouverture du fichier !\n");
        exit(1);
    }
    fprintf(file, "%d\n", nbCandidats);
    for (int i = 0; i < nbCandidats; i++)
    {
        fprintf(file, "%d\n%s\n%s\n", tetud[i]->num, tetud[i]->nom, tetud[i]->prenom);
        for (int note = 0; note < 4; note++)
        {
            fprintf(file, "%.2f\t", tetud[i]->tabNotes[note]);
        }
        fprintf(file, "\n%d\n", tetud[i]->nbChoix);
        sauvegarderChoix(tetud[i]->lChoix, file);
    }
    fclose(file);
}

/**
 * @brief Sauvegarde les choix d'un candidat
 *
 * @author Antoine Perederii
 * @param lChoix Liste des choix
 * @param file Fichier dans lequel sauvegarder
 */
void sauvegarderChoix(ListeChoix lChoix, FILE *file)
{
    if (lChoix != NULL)
    {
        fprintf(file, "%s\n%s\n%d\n%d\n", lChoix->choix.ville, lChoix->choix.departement, lChoix->choix.decisionAdmission, lChoix->choix.decisionCandidat);
        sauvegarderChoix(lChoix->suiv, file);
    }
}

/**
 * @brief Sauvegarde tous les candidats
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void sauvegardeTousCandidats(Etudiant *tetud[], int nbCandidats)
{
    sauvegarderCandidats(tetud, nbCandidats, "candidats.txt");
}
/**
 * @brief Sauvegarde le tableau des candidats admis
 *
 * @author Kyllian Chabanon
 * @param tetudAdmis Liste des étudiants admis
 * @param nbEtudAdmis Nombre d'étudiants admis
 */
void sauvegardeAdmis(Etudiant *tetudAdmis[], int nbEtudAdmis)
{
    sauvegarderCandidats(tetudAdmis, nbEtudAdmis, "candidatsAdmis.txt");
}

/**
 * @brief Sauvegarde le tableau des candidats en attente
 *
 * @author Kyllian Chabanon
 * @param tetudAttente Liste des étudiants en attente
 * @param nbEtudAttente Nombre d'étudiants en attente
 */
void sauvegardeAttente(Etudiant *tetudAttente[], int nbEtudAttente)
{
    sauvegarderCandidats(tetudAttente, nbEtudAttente, "candidatsAttente.txt");
}

/**
 * @brief Libère l'espace alloué au tableau des candidats
 * 
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void libererEspaceCandidats(Etudiant *tetud[], int nbCandidats)
{
    for (int i = 0; i < nbCandidats; i++)
    {
        free(tetud[i]);
    }
}

/**
 * @brief Libère l'espace alloué au tableau des IUT
 * 
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void libererEspaceIUT(VilleIUT *tiut[], int nbIUT)
{
    for (int i = 0; i < nbIUT; i++)
    {
        while (!vide(tiut[i]->ldept))
        {
            supprimerEnTete(tiut[i]->ldept);
        }
        free(tiut[i]);
    }
}