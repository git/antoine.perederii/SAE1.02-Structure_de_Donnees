/**
 * @file administrateur.c
 * @author Kyllian Chabanon
 * @brief Fichier contenant les fonctions des administrateurs
 * 
 */

#include "SAE.h"

/**
 * @brief Permet de modifier le nombre de places dans un département
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void modificationNbPDept(VilleIUT *tiut[], int nbIUT)
{
    char iutModif[30], deptModif[30];
    int posIUT, posDept, nbPlaces;
    bool trouve;
    printf("Dans quel IUT se situe le département que vous voulez modifier ?\n> ");
    scanf("%s", iutModif);
    posIUT = rechercheVille(tiut, nbIUT, iutModif, &trouve);
    if (trouve == false)
    {
        printf("Cet IUT n'existe pas. Veuillez réessayer.\n");
        return;
    }
    printf("Quel est le département dont vous voulez modifier le nombre de places ?\n> ");
    scanf("%s", deptModif);
    posDept = rechercheDept(tiut[posIUT]->ldept, deptModif, &trouve);
    if (trouve == false)
    {
        printf("Ce département n'existe pas dans cet IUT. Veuillez réessayer.\n");
        return;
    }
    printf("Il y a actuellement %d places dans ce département. Entrez le nouveau nombre de places :\n> ", getNbP(tiut[posIUT]->ldept, posDept));
    scanf("%d", &nbPlaces);
    setNbP(tiut[posIUT]->ldept, posDept, nbPlaces);
    printf("Le nombre de places est bien passé à %d.\n", getNbP(tiut[posIUT]->ldept, posDept));
}

/**
 * @brief Permet de créer un département dans un IUT, et un IUT s'il n'existe pas
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void creationDept(VilleIUT *tiut[], int nbIUT)
{
    char iut[30], nomDept[30], nomResp[30];
    int posIUT, nbP;
    bool trouve;
    printf("Dans quel IUT voulez-vous créer un département ?\n> ");
    scanf("%s", iut);
    posIUT = rechercheVille(tiut, nbIUT, iut, &trouve);
    if (trouve == false)
    {
        printf("Cet IUT n'existe pas. Veuillez réessayer.\n");
        return;
    }
    printf("Entrez le nom du département que vous voulez créer :\n> ");
    scanf("%s", nomDept);
    rechercheDept(tiut[posIUT]->ldept, nomDept, &trouve);
    if (trouve == true)
    {
        printf("Ce département existe déjà.\n");
        return;
    }
    printf("Entrez le nombre de places dans le département :\n> ");
    scanf("%d%*c", &nbP);
    printf("Entrez le nom et prénom du responsable :\n> ");
    fgets(nomResp, 30, stdin);
    nomResp[strlen(nomResp) - 1] = '\0';
    tiut[posIUT]->ldept = inserer(tiut[posIUT]->ldept, nomDept, nbP, nomResp);
    printf("Vous avez créé le département \"%s\", avec %d places. Son responsable est %s.\n", nomDept, nbP, nomResp);
}

/**
 * @brief Permet de modifier le responsable d'un département
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void modificationRespDept(VilleIUT *tiut[], int nbIUT)
{
    char iutModif[30], deptModif[30], nouvResp[30];
    int posIUT, posDept;
    bool trouve;
    printf("Dans quel IUT se situe le département que vous voulez modifier ?\n> ");
    scanf("%s", iutModif);
    posIUT = rechercheVille(tiut, nbIUT, iutModif, &trouve);
    if (trouve == false)
    {
        printf("Cet IUT n'existe pas. Veuillez réessayer.\n");
        return;
    }
    printf("Quel est le département dont vous voulez modifier le responsable ?\n> ");
    scanf("%s%*c", deptModif);
    posDept = rechercheDept(tiut[posIUT]->ldept, deptModif, &trouve);
    if (trouve == false)
    {
        printf("Ce département n'existe pas dans cet IUT. Veuillez réessayer.\n");
        return;
    }

    printf("Le responsable de ce département est actuellement %s. Entrez le nouveau responsable :\n> ", getResp(tiut[posIUT]->ldept, posDept));
    fgets(nouvResp, 30, stdin);
    nouvResp[strlen(nouvResp) - 1] = '\0';
    setResp(tiut[posIUT]->ldept, posDept, nouvResp);
    printf("Le nouveau responsable est %s.\n", getResp(tiut[posIUT]->ldept, posDept));
}

/**
 * @brief Lance la phase de candidatures et arrête celle de traitement
 *
 * @author Kyllian Chabanon
 * @param phaseCandidature Pointeur sur un booléen de l'état de la phase de candidatures
 * @param phaseTraitement Pointeur sur un booléen de l'état de la phase de traitement
 */
void lancerPhaseCandidature(bool *phaseCandidature, bool *phaseTraitement)
{
    if (*phaseCandidature == false)
    {
        printf("La phase de candidature est maintenant ouverte.\n");
        *phaseCandidature = true;
        *phaseTraitement = false;
    }
    else
    {
        printf("La phase de candidature est déjà ouverte.\n");
    }
}

/**
 * @brief Lance la phase de traitement et arrête celle de candidature
 *
 * @author Kyllian Chabanon
 * @param phaseCandidature Pointeur sur un booléen de l'état de la phase de candidatures
 * @param phaseTraitement Pointeur sur un booléen de l'état de la phase de traitement
 */
void stopperPhaseCandidature(bool *phaseCandidature, bool *phaseTraitement)
{
    if (*phaseCandidature == true)
    {
        printf("La phase de candidature est maintenant fermée.\n");
        *phaseCandidature = false;
        *phaseTraitement = true;
    }
    else
    {
        printf("La phase de candidature est déjà fermée.\n");
    }
}