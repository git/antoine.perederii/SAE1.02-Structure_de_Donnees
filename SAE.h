#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "structures.h"

void global(void);

/* Menus */
void menu(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats, bool *phaseCandidatures, bool *phaseTraitement, Etudiant *tetudAdmis[], int *nbEtudAdmis, Etudiant *tetudAttente[], int *nbEtudAttente);
void menuAdministrateur(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats, bool *phaseCandidatures, bool *phaseTraitement);
void menuUtilisateur(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats);
void menuCandidat(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats);
void menuResponsable(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int *nbIUT, Etudiant *tetudAdmis[], int *nbEtudAdmis, Etudiant *tetudAttente[], int *nbEtudAttente);
void menuAffichage(Etudiant *tetudResp[], int nbCandidatsDept, int *nbCandidatsAdmis, int *nbCandidatsAttente);
void menuPostCandidat(Etudiant *tetud[], int *nbCandidats);


/* Partie 1 */
/* Fichier */
int chargementVillesIUT(VilleIUT *tiut[]);
int rechercheVille(VilleIUT *tiut[], int nbIUT, char val[], bool *trouve);
void sauvegardeVillesIUT(VilleIUT *tiut[], int nbIUT);

/* Utilisateur */
void affichageVillesIUT(VilleIUT *tiut[], int nbIUT);
void affichageDeptIUT(VilleIUT *tiut[], int nbIUT);
void affichageNbP(VilleIUT *tiut[], int nbIUT);
void affichageDeptParticulier(VilleIUT *tiut[], int nbIUT);

/* Administrateur */
void modificationNbPDept(VilleIUT *tiut[], int nbIUT);
void creationDept(VilleIUT *tiut[], int nbIUT);
int suppressionDept(VilleIUT *tiut[], int nbIUT);
void modificationRespDept(VilleIUT *tiut[], int nbIUT);
void lancerPhaseCandidature(bool *phaseCandidatures, bool *phaseTraitement);
void stopperPhaseCandidature(bool *phaseCandidatures, bool *phaseTraitement);

/* Partie 2 */
int rechercheCandidat(Etudiant *tetud[], int nbCandidats, int numRecherche, bool *trouve);
void afficherEtudiant(Etudiant mEtudiant);
void afficherCandidat(Etudiant *tetud[], int nbCandidats);
void afficherCandidats(Etudiant *tetud[], int nbCandidats);
int chargerCandidats(Etudiant *tetud[]);
void sauvegarderCandidats(Etudiant *tetud[], int nbCandidats, char nomFichier[]);
void sauvegarderChoix(ListeChoix lChoix, FILE *flot);

void modificationNbPDeptResp(VilleIUT *tiut[], int nbIUT, char respVille[30], char respDept[30]);
void afficherCandidatsAdmis(Etudiant *tetud[], int nbCandidats);
void afficherCandidatsRefuses(Etudiant *tetud[], int nbCandidats);
void afficherCandidatsAttente(Etudiant *tetud[], int nbCandidats);
Etudiant modifStatueCandidat(Etudiant *tetud[], int nbCandidats, float noteMin[], VilleIUT *tiut[], int nbIUT, char respVille[30], char respDept[30]);
void statueCandidat(Etudiant *tetud[], int nbCandidats, Etudiant *tetudAdmis[], Etudiant *tetudAttente[], int *nbEtudAdmis, int *nbEtudAttente);
int nbCandidatsAdmis(Etudiant *tetud[], int nbCandidats);
int nbCandidatsRefuses(Etudiant *tetud[], int nbCandidats);
int nbCandidatsAttente(Etudiant *tetud[], int nbCandidats);
float modifNoteMin(float noteMin[]);
void ajouterCandidature(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int nbVillesIUT);
void supprimerCandidature(Etudiant *tetud[], int nbCandidats);
int inscription(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int nbVillesIUT);


int plusPetit(Etudiant *tetudResp[], int nbCandResp);
void echanger(Etudiant *tetudResp[], int i, int j);
void triNote(Etudiant *tetudResp[], int nbCandResp);
void deptResp(VilleIUT *tiut[], int nbVilles, char respVille[], char respIUT[]);
int chargementRespDept(Etudiant *tetud[], int nbCand, char respVille[], char respIUT[], Etudiant *tetudResp[]);
void sauvegardeAdmis(Etudiant *tetudAdmis[], int nbEtudAdmis);
void sauvegardeAttente(Etudiant *tetudAttente[], int nbEtudAttente);
void sauvegardeTousCandidats(Etudiant *tetud[], int nbCandidats);
void libererEspaceCandidats(Etudiant *tetud[], int nbCandidats);
void libererEspaceIUT(VilleIUT *tiut[], int nbIUT);