/**
 * @file menus.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant tous les différents menus de notre application.
 *
 */

#include "SAE.h"

/**
 * @brief Fonction de sélection des menus
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param phaseCandidatures Booléen contenant l'état de la phase de candidature
 * @param phaseTraitement Booléen contenant l'état de la phase d'admission
 * @param tetudAdmis Tableau des étudiants admis
 * @param nbEtudAdmis Nombre de candidats admis
 * @param tetudAttente Tableau des étudiants en file d'attente
 * @param nbEtudAttente Nombre d'étudiants en file d'attente
 */
void menu(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats, bool *phaseCandidatures, bool *phaseTraitement, Etudiant *tetudAdmis[], int *nbEtudAdmis, Etudiant *tetudAttente[], int *nbEtudAttente)
{
    int choix, pasMdP = 0;
    bool c = false;
    char etat[8], mdp[30], MdPAdmin[30];
    while (c == false)
    {
        if (*phaseCandidatures == true)
        {
            strcpy(etat, "ouverte");
        }
        else
        {
            strcpy(etat, "fermée");
        }
        printf("\n\nVeuillez choisir votre menu :\n");
        printf("\t1 - Menu administrateur\n");
        printf("\t2 - Menu utilisateur\n");
        printf("\t3 - Menu candidat (phase de candidature %s)\n", etat);
        printf("\t4 - Menu responsable de département\n");
        printf("\t5 - Menu POST Candidatures\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            menuAdministrateur(tiut, nbIUT, tetud, nbCandidats, phaseCandidatures, phaseTraitement);
            break;
        case 2:
            menuUtilisateur(tiut, nbIUT, tetud, nbCandidats);
            break;
        case 3:
            if (phaseCandidatures == false)
            {
                printf("La phase de candidature est actuellement fermée.");
                break;
            }
            menuCandidat(tiut, nbIUT, tetud, nbCandidats);
            break;
        case 4:
            menuResponsable(tetud, *nbCandidats, tiut, nbIUT, tetudAdmis, nbEtudAdmis, tetudAttente, nbEtudAttente);
            break;
        case 5:
            menuPostCandidat(tetud, nbCandidats);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

/**
 * @brief Menu des administrateurs
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param phaseCandidatures Booléen contenant l'état de la phase de candidature
 * @param phaseTraitement Booléen contenant l'état de la phase d'admission
 */
void menuAdministrateur(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats, bool *phaseCandidatures, bool *phaseTraitement)
{
    int choix;
    bool c = false;
    while (c == false)
    {
        printf("\n\nMenu d'administrateur : Que voulez-vous faire ?\n");
        printf("\t1 - Modifier le nombre de places dans un département\n");
        printf("\t2 - Créer un département dans un IUT\n");
        printf("\t3 - Supprimer un département d'un IUT\n");
        printf("\t4 - Modifier le responsable d'un département\n");
        printf("\t5 - Afficher les informations de tous les candidats\n");
        printf("\t6 - Afficher les informations d'un seul candidat\n");
        printf("\t7 - Lancer la phase de candidature\n");
        printf("\t8 - Stopper la phase de candidature\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            modificationNbPDept(tiut, *nbIUT);
            break;
        case 2:
            creationDept(tiut, *nbIUT);
            break;
        case 3:
            *nbIUT = suppressionDept(tiut, *nbIUT);
            break;
        case 4:
            modificationRespDept(tiut, *nbIUT);
            break;
        case 5:
            afficherCandidats(tetud, *nbCandidats);
            printf("\nNombre de candidats : %d\n", *nbCandidats);
            break;
        case 6:
            afficherCandidat(tetud, *nbCandidats);
            break;
        case 7:
            lancerPhaseCandidature(phaseCandidatures, phaseTraitement);
            break;
        case 8:
            stopperPhaseCandidature(phaseCandidatures, phaseTraitement);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

/**
 * @brief Menu des utilisateurs
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void menuUtilisateur(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats)
{
    int choix;
    bool c = false;
    while (c == false)
    {
        printf("\n\nMenu d'utilisateur : Que voulez-vous faire ?\n");
        printf("\t1 - Voir les villes possédant un IUT\n");
        printf("\t2 - Voir les départements dans chaque IUT\n");
        printf("\t3 - Voir le nombre de places en première année\n");
        printf("\t4 - Voir les IUT possédant un département particulier\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            affichageVillesIUT(tiut, *nbIUT);
            break;
        case 2:
            affichageDeptIUT(tiut, *nbIUT);
            break;
        case 3:
            affichageNbP(tiut, *nbIUT);
            break;
        case 4:
            affichageDeptParticulier(tiut, *nbIUT);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

/**
 * @brief Menu des candidats
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void menuCandidat(VilleIUT *tiut[], int *nbIUT, Etudiant *tetud[], int *nbCandidats)
{
    int choix;
    bool c = false;
    while (c == false)
    {
        printf("\n\nMenu candidat : Que voulez-vous faire ?\n");
        printf("\t1 - S'inscrire\n");
        printf("\t2 - Afficher ses informations\n");
        printf("\t3 - Ajouter un choix\n");
        printf("\t4 - Supprimer un choix\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            *nbCandidats = inscription(tetud, *nbCandidats, tiut, *nbIUT);
            break;
        case 2:
            afficherCandidat(tetud, *nbCandidats);
            break;
        case 3:
            ajouterCandidature(tetud, *nbCandidats, tiut, *nbIUT);
            break;
        case 4:
            supprimerCandidature(tetud, *nbCandidats);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

/**
 * @brief Menu des responsables
 *
 * @author Kyllian Chabanon - Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetudAdmis Tableau des étudiants admis
 * @param nbEtudAdmis Nombre d'étudiants admis
 * @param tetudAttente Tableau des étudiants en file d'attente
 * @param nbEtudAttente Nombre d'étudiants en file d'attente
 */
void menuResponsable(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int *nbIUT, Etudiant *tetudAdmis[], int *nbEtudAdmis, Etudiant *tetudAttente[], int *nbEtudAttente)
{
    int choix;
    bool c = false;
    float noteMin[5];
    Etudiant *tetudResp[100];
    int nbCandidatsDept;
    char respVille[30], respDept[30];
    deptResp(tiut, *nbIUT, respVille, respDept);
    nbCandidatsDept = chargementRespDept(tetud, nbCandidats, respVille, respDept, tetudResp);
    triNote(tetudResp, nbCandidatsDept);
    while (c == false)
    {
        printf("\n\nMenu des Responsable de Departement : Que voulez-vous faire ?\n");
        printf("\t1 - Affichages\n");
        printf("\t2 - Modifier le nombre de places dans un département\n");
        printf("\t3 - Modifier la note minimal d'admission du departement\n");
        printf("\t4 - Lancer les admissions\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            menuAffichage(tetudResp, nbCandidatsDept, nbEtudAdmis, nbEtudAttente);
            break;
        case 2:
            modificationNbPDeptResp(tiut, *nbIUT, respVille, respDept);
            break;
        case 3:
            *noteMin = modifNoteMin(noteMin);
            break;
        case 4:
            modifStatueCandidat(tetudResp, nbCandidatsDept, noteMin, tiut, *nbIUT, respVille, respDept);
            statueCandidat(tetudResp, nbCandidatsDept, tetudAdmis, tetudAttente, nbEtudAdmis, nbEtudAttente);
            afficherCandidats(tetudAdmis, *nbEtudAdmis);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

/**
 * @brief Menu d'affichage des responsables
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param tetudAdmis Tableau des étudiants admis
 * @param nbEtudAdmis Nombre d'étudiants admis
 * @param tetudAttente Tableau des étudiants en file d'attente
 * @param nbEtudAttente Nombre d'étudiants en file d'attente
 */
void menuAffichage(Etudiant *tetudResp[], int nbEtudDept, int *nbEtudAdmis, int *nbEtudAttente)
{
    int choix;
    bool c = false;
    int nbEtudRefuses;
    while (c == false)
    {
        printf("\n\nMenu des Affichages Responsable de Departement : Que voulez-vous faire ?\n");
        printf("\t1 - Afficher les candidats dans mon département (trier par note moyenne)\n");
        printf("\t2 - Afficher les candidats admis du departement\n");
        printf("\t3 - Afficher les candidats refusés du departement\n");
        printf("\t4 - Afficher les candidats en attente du departement\n");
        printf("\t5 - Afficher un seul candidat du departement\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            afficherCandidats(tetudResp, nbEtudDept);
            printf("Il y a %d candidats dans votre departement.\n", nbEtudDept);
            break;
        case 2:
            afficherCandidatsAdmis(tetudResp, nbEtudDept);
            printf("Il y a %d candidats admis dans votre departement.\n", *nbEtudAdmis);
            break;
        case 3:
            nbEtudRefuses = nbCandidatsRefuses(tetudResp, nbEtudDept);
            afficherCandidatsRefuses(tetudResp, nbEtudDept);
            printf("Il y a %d candidats refusés dans votre departement.\n", nbEtudRefuses);
            break;
        case 4:
            afficherCandidatsAttente(tetudResp, nbEtudDept);
            printf("Il y a %d candidats en attente dans votre departement.\n", *nbEtudAttente);
            break;
        case 5:
            afficherCandidat(tetudResp, nbEtudDept);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}

void menuPostCandidat(Etudiant *tetud[], int *nbCandidats)
{
    int choix;
    bool c = false;
    while (c == false)
    {
        printf("\n\nMenu des Affichages Responsable de Departement : Que voulez-vous faire ?\n");
        printf("\t1 - Afficher les informations sur mes voeus\n");
        printf("\t9 - Quitter\n");
        printf("\nEntrez votre choix :\n> ");
        scanf("%d%*c", &choix);
        switch (choix)
        {
        case 1:
            afficherCandidat(tetud, *nbCandidats);
            break;
        case 9:
            c = true;
            return;
        default:
            printf("Option non reconnue. Veuillez recommencer.\n");
            break;
        }
    }
}