/**
 * @file suppression.c
 * @author Kyllian Chabanon
 * @brief Fichier contenant les fonctions de suppression
 */

#include "SAE.h"

/**
 * @brief Supprime un choix d'un candidat
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void supprimerCandidature(Etudiant *tetud[], int nbCandidats)
{
    int num, posCand, posVille;
    char ville[30], dept[30];
    bool trouve;
    Choix c;
    printf("Entrez votre numéro de candidat :\n> ");
    scanf("%d", &num);
    posCand = rechercheCandidat(tetud, nbCandidats, num, &trouve);
    if (!trouve)
    {
        printf("Numéro de candidat non trouvé.\n");
        return;
    }
    printf("Entrez la ville du département que vous souhaitez supprimer :\n> ");
    scanf("%s", c.ville);
    printf("Entrez le département que vous souhaitez supprimer :\n> ");
    scanf("%s", c.departement);
    trouve = rechercheChoix(tetud[posCand]->lChoix, c);
    if (!trouve)
    {
        printf("Choix non trouvé.\n");
        return;
    }
    tetud[posCand]->lChoix = supprimerChoix(tetud[posCand]->lChoix, c);
    tetud[posCand]->nbChoix--;
}

/**
 * @brief Permet de supprimer un département, et un IUT s'il n'a plus aucun département
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @return int
 */
int suppressionDept(VilleIUT *tiut[], int nbIUT)
{
    char nomDept[30], iut[30], choix;
    int posIUT;
    bool trouve;
    printf("Dans quel IUT voulez-vous supprimer un département ?\n> ");
    scanf("%s", iut);
    posIUT = rechercheVille(tiut, nbIUT, iut, &trouve);
    if (trouve == false)
    {
        printf("Cet IUT n'existe pas. Veuillez réessayer.\n");
        return nbIUT;
    }
    printf("Quel département voulez-vous supprimer ?\n> ");
    scanf("%s", nomDept);
    rechercheDept(tiut[posIUT]->ldept, nomDept, &trouve);
    if (trouve == false)
    {
        printf("Ce département n'existe pas dans cet IUT. Veuillez réessayer.\n");
        return nbIUT;
    }
    printf("Voulez-vous vraiment supprimer le département %s de l'IUT %s ? (o/N)\n> ", nomDept, iut);
    scanf(" %c%*c", &choix);
    if (choix == 'o' || choix == 'O')
    {
        tiut[posIUT]->ldept = supprimer(tiut[posIUT]->ldept, nomDept);
        printf("Vous avez bien supprimé le département %s de l'IUT %s.\n", nomDept, iut);
        if (vide(tiut[posIUT]->ldept))
        {
            for (int i = posIUT; i < nbIUT; i++)
            {
                tiut[i] = tiut[i + 1];
            }
            free(tiut[nbIUT]);
            printf("L'IUT %s a été supprimé car il n'avait plus de départements.\n", iut);
            return nbIUT - 1;
        }
    }
    else
    {
        printf("Vous avez annulé la suppression du département.\n");
    }
    return nbIUT;
}