/**
 * @file recherche.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant les fonctions de recherche
 *
 */

#include "SAE.h"

/**
 * @brief Retourne la position de l'IUT recherché et un booléen qui prend la valeur true si l'élément a été trouvé et false sinon
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nb Nombre d'IUT
 * @param val Ville recherchée
 * @param trouve Pointeur sur un booléen qui prend la valeur true si l'élément recherché a été trouvé et false sinon
 * @return int
 */
int rechercheVille(VilleIUT *tiut[], int nb, char val[], bool *trouve)
{
    int i;
    for (i = 0; i < nb; i++)
    {
        if (strcmp(tiut[i]->ville, val) == 0)
        {
            *trouve = true;
            return i;
        }
    }
    *trouve = false;
    return i;
}

/**
 * @brief Retourne la position du candidat recherché et un booléen qui prend la valeur true si l'élément a été trouvé et false sinon
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param numRecherche Numéro du candidat à rechercher
 * @param trouve Pointeur sur un booléen qui prend la valeur true si l'étudiant a été trouvé et false sinon
 * @return int
 */
int rechercheCandidat(Etudiant *tetud[], int nbCandidats, int numRecherche, bool *trouve)
{
    int i;
    for (i = 0; i < nbCandidats; i++)
    {
        if (numRecherche == tetud[i]->num)
        {
            *trouve = true;
            return i;
        }
    }
    *trouve = false;
    return i;
}