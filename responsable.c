/**
 * @file responsable.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant les fonctions du responsable de département
 *
 */

#include "SAE.h"

/**
 * @brief Renvoie par adresse le département et la ville du département duquel le responsable s'occupe
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbVilles Nombre des IUT
 * @param respVille Ville du responsable
 * @param respIUT IUT du responsable
 */
void deptResp(VilleIUT *tiut[], int nbVilles, char respVille[], char respIUT[])
{
    char nom[30];
    printf("Entrez votre nom et votre prénom :\n> ");
    fgets(nom, 30, stdin);
    nom[strlen(nom) - 1] = '\0';
    for (int i = 0; i < nbVilles; i++)
    {
        for (int n = 0; n < longueur(tiut[i]->ldept); n++)
        {
            if (strcmp(getResp(tiut[i]->ldept, n), nom) == 0)
            {
                strcpy(respVille, tiut[i]->ville);
                strcpy(respIUT, getDept(tiut[i]->ldept, n));
                return;
            }
        }
    }
}

/**
 * @brief Charge le tableau des candidats ayant candidatés dans le département du responsable
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCand Nombre de candidats
 * @param respVille Ville du responsable
 * @param respIUT IUT du responsable
 * @param tetudResp Tableau des candidats dans le département du responsable
 * @return int
 */
int chargementRespDept(Etudiant *tetud[], int nbCand, char respVille[], char respIUT[], Etudiant *tetudResp[])
{
    int nb = 0;
    for (int i = 0; i < nbCand; i++)
    {
        for (int n = 0; n < tetud[i]->nbChoix; n++)
        {
            if (strcmp(getDeptChoix(tetud[i]->lChoix, n), respIUT) == 0 && strcmp(getVilleChoix(tetud[i]->lChoix, n), respVille) == 0)
            {
                tetudResp[nb] = tetud[i];
                nb++;
            }
        }
    }
    return nb;
}

/**
 * @brief Modification du statut d'un candidat
 * 
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param noteMin Tableau de notes minimales
 * @param tiut Tableau d'IUT
 * @param nbIUT Nombre d'IUT
 * @param respVille Ville du responsable
 * @param respDept Département du responsable
 * @return Etudiant 
 */
Etudiant modifStatueCandidat(Etudiant *tetud[], int nbCandidats, float noteMin[], VilleIUT *tiut[], int nbIUT, char respVille[30], char respDept[30]) //! tableau des admis + compteur admis
{
    int posIUT, posDept, nbPlaces;
    bool trouve;
    posIUT = rechercheVille(tiut, nbIUT, respVille, &trouve);
    if (trouve == false)
    {
        printf("L'IUT de ce responsable n'existe pas. Veuillez réessayer en changeant de responsable.\n");
        return **tetud;
    }
    posDept = rechercheDept(tiut[posIUT]->ldept, respDept, &trouve);
    if (trouve == false)
    {
        printf("Le departement de ce responsable n'existe pas. Veuillez réessayer en changeant de responsable.\n");
        return **tetud;
    }
    for (int i = 0; i < nbCandidats; i++)
    {
        if (tetud[i]->tabNotes[maths] >= noteMin[maths] && tetud[i]->tabNotes[francais] >= noteMin[francais] && tetud[i]->tabNotes[anglais] >= noteMin[anglais] && tetud[i]->tabNotes[spe] >= noteMin[spe] && tetud[i]->tabNotes[moy] >= noteMin[moy] && nbCandidatsAdmis(tetud, nbCandidats) <= getNbP(tiut[posIUT]->ldept, posDept))
        {
            tetud[i]->lChoix->choix.decisionAdmission = 1;
        }
        else if (tetud[i]->tabNotes[maths] >= noteMin[maths] && tetud[i]->tabNotes[francais] >= noteMin[francais] && tetud[i]->tabNotes[anglais] >= noteMin[anglais] && tetud[i]->tabNotes[spe] >= noteMin[spe] && tetud[i]->tabNotes[moy] >= noteMin[moy] && nbCandidatsAdmis(tetud, nbCandidats) > getNbP(tiut[posIUT]->ldept, posDept))
        {
            tetud[i]->lChoix->choix.decisionAdmission = 2;
        }
        else
        {
            tetud[i]->lChoix->choix.decisionAdmission = -1;
        }
    }
    return **tetud;
}

/**
 * @brief Ajoute les candidats dans les tableaux des candidats admis et en attente
 * 
 * @author Kyllian Chabanon - Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param tetudAdmis Tableau des candidats admis
 * @param tetudAttente Tableau des candidats en attente
 * @param nbEtudAdmis Nombre d'étudiants admis
 * @param nbEtudAttente Nombre d'étudiants en attente
 */
void statueCandidat(Etudiant *tetud[], int nbCandidats, Etudiant *tetudAdmis[], Etudiant *tetudAttente[], int *nbEtudAdmis, int *nbEtudAttente)
{
    int i;
    for (i = 0; i < nbCandidats; i++)
    {
        if (tetud[i]->lChoix->choix.decisionAdmission == 1)
        {
            tetudAdmis[*nbEtudAdmis] = tetud[i];
            *nbEtudAdmis++;
        }
        else if (tetud[i]->lChoix->choix.decisionAdmission == 2)
        {
            tetudAttente[*nbEtudAttente] = tetud[i];
            *nbEtudAttente++;
        }
    }
}

/**
 * @brief Retourne le nombre de candidats admis
 * 
 * @author Antoine Perederii
 * @param tetud Tableau de candidats
 * @param nbCandidats Nombre de candidats
 * @return int 
 */
int nbCandidatsAdmis(Etudiant *tetud[], int nbCandidats)
{
    int nbAdmis = 0;
    for (int i = 0; i < nbCandidats; i++)
    {
        if (tetud[i]->lChoix->choix.decisionAdmission == 1)
        {
            nbAdmis++;
        }
    }
    return nbAdmis;
}

/**
 * @brief Retourne le nombre de candidats refusés
 * 
 * @author Antoine Perederii
 * @param tetud Tableau de candidats
 * @param nbCandidats Nombre de candidats
 * @return int 
 */
int nbCandidatsRefuses(Etudiant *tetud[], int nbCandidats)
{
    int nbRefuses = 0;
    for (int i = 0; i < nbCandidats; i++)
    {
        if (tetud[i]->lChoix->choix.decisionAdmission == -1)
        {
            nbRefuses++;
        }
    }
    return nbRefuses;
}

/**
 * @brief Retourne le nombre de candidats en attente
 * 
 * @author Antoine Perederii
 * @param tetud Tableau de candidats
 * @param nbCandidats Nombre de candidats
 * @return int 
 */
int nbCandidatsAttente(Etudiant *tetud[], int nbCandidats)
{
    int nbAttente = 0;
    for (int i = 0; i < nbCandidats; i++)
    {
        if (tetud[i]->lChoix->choix.decisionAdmission == 2)
        {
            nbAttente++;
        }
    }
    return nbAttente;
}

/**
 * @brief Retourne les notes minimales
 * 
 * @author Antoine Perederii
 * @param noteMin Tableau de nombre flottants contenant les notes minimales
 * @return float
 */
float modifNoteMin(float noteMin[])
{
    for (int i = 0; i < 5; i++)
    {
        printf("Entrez la note minimale pour la matiere %d (0/20, -1 pour sortir) : ", i + 1);
        scanf("%f", &noteMin[i]);
        while (noteMin[i] < 0 || noteMin[i] > 20)
        {
            scanf("%f", &noteMin[i]);
            if (noteMin[i] == -1)
            {
                break;
            }
            printf("La note doit être comprise entre 0 et 20 !\n");
        }
    }
    return *noteMin;
}

/**
 * @brief Modifie le nombre de places du département
 * 
 * @author Kyllian Chabanon - Antoine Perederii
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 * @param respVille Ville du responsable
 * @param respDept Département du responsable
 */
void modificationNbPDeptResp(VilleIUT *tiut[], int nbIUT, char respVille[30], char respDept[30])
{
    int posIUT, posDept, nbPlaces;
    bool trouve;
    posIUT = rechercheVille(tiut, nbIUT, respVille, &trouve);
    if (trouve == false)
    {
        printf("L'IUT de ce responsable n'existe pas. Veuillez réessayer en changeant de responsable.\n");
        return;
    }
    posDept = rechercheDept(tiut[posIUT]->ldept, respDept, &trouve);
    if (trouve == false)
    {
        printf("Le departement de ce responsable n'existe pas. Veuillez réessayer en changeant de responsable.\n");
        return;
    }
    printf("Il y a actuellement %d places dans ce département. Entrez le nouveau nombre de places :\n> ", getNbP(tiut[posIUT]->ldept, posDept));
    scanf("%d", &nbPlaces);
    setNbP(tiut[posIUT]->ldept, posDept, nbPlaces);
    printf("Le nombre de places est bien passé à %d.\n", getNbP(tiut[posIUT]->ldept, posDept));
}

/**
 * @brief Trie le tableau des candidats par moyenne
 * 
 * @author Kyllian Chabanon
 * @param tetudResp Tableau des candidats
 * @param nbCandResp Nombre de candidats
 */
void triNote(Etudiant *tetudResp[], int nbCandResp)
{
    int min;
    while (nbCandResp > 1)
    {
        min = plusPetit(tetudResp, nbCandResp);
        echanger(tetudResp, min, nbCandResp - 1);
        nbCandResp--;
    }
}

/**
 * @brief Permute deux cases d'un tableau
 * 
 * @author Kyllian Chabanon
 * @param tetudResp Tableau des candidats
 * @param i Première case
 * @param j Seconde case
 */
void echanger(Etudiant *tetudResp[], int i, int j)
{
    Etudiant *aux;
    aux = tetudResp[i];
    tetudResp[i] = tetudResp[j];
    tetudResp[j] = aux;
}

/**
 * @brief Retourne l'emplacement du plus petit élément dans un tableau
 * 
 * @author Kyllian Chabanon
 * @param tetudResp Tableau des candidats
 * @param nbCandResp Nombre de candidats
 * @return int 
 */
int plusPetit(Etudiant *tetudResp[], int nbCandResp)
{
    int min = 0;
    for (int i = 0; i < nbCandResp; i++)
    {
        if (tetudResp[i]->tabNotes[4] < tetudResp[min]->tabNotes[4])
        {
            min = i;
        }
    }
    return min;
}