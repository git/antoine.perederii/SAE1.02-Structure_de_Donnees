/**
 * @file candidatures.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant les fonctions pour gérer les candidatures
 *
 */
#include "SAE.h"

/*  Partie 2    */

/**
 * @brief Ajoute une candidature à un candidat
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param tiut Tableau des IUT
 * @param nbVillesIUT Nombre d'IUT
 */
void ajouterCandidature(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int nbVillesIUT)
{
    int num, posCand, posVille;
    char ville[30], dept[30];
    bool trouve;
    Choix c;
    printf("Entrez votre numéro de candidat :\n> ");
    scanf("%d", &num);
    posCand = rechercheCandidat(tetud, nbCandidats, num, &trouve);
    if (!trouve)
    {
        printf("Numéro de candidat non trouvé.\n");
        return;
    }
    printf("Entrez la ville du département que vous souhaitez ajouter :\n> ");
    scanf("%s", ville);
    posVille = rechercheVille(tiut, nbVillesIUT, ville, &trouve);
    if (!trouve)
    {
        printf("Ville non trouvée.\n");
        return;
    }
    printf("Entrez le département que vous souhaitez ajouter :\n> ");
    scanf("%s", dept);
    rechercheDept(tiut[posVille]->ldept, dept, &trouve);
    if (!trouve)
    {
        printf("Département non trouvé.\n");
        return;
    }
    strcpy(c.ville, ville);
    strcpy(c.departement, dept);
    c.decisionAdmission = 0;
    c.decisionCandidat = 0;
    tetud[posCand]->lChoix = insererChoix(tetud[posCand]->lChoix, c);
    tetud[posCand]->nbChoix++;
}

/**
 * @brief Permet à un utilisateur de s'inscrire en tant que candidat
 *
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 * @param tiut Tableau des IUT
 * @param nbVillesIUT Nombre d'IUT
 * @return int
 */
int inscription(Etudiant *tetud[], int nbCandidats, VilleIUT *tiut[], int nbVillesIUT)
{
    Etudiant cand;
    Choix c;
    bool trouve;
    int posVille;
    printf("Quel est votre nom ?\n> ");
    scanf("%s", cand.nom);
    printf("Quel est votre prénom ?\n> ");
    scanf("%s", cand.prenom);
    printf("Entrez vos notes en : mathématiques, français, anglais et en spécialité :\n> ");
    scanf("%f %f %f %f", &cand.tabNotes[0], &cand.tabNotes[1], &cand.tabNotes[2], &cand.tabNotes[3]);
    printf("Entrez votre premier choix :\n");
    printf("Ville :\n> ");
    scanf("%s", c.ville);
    posVille = rechercheVille(tiut, nbVillesIUT, c.ville, &trouve);
    if (!trouve)
    {
        printf("Ville non trouvée.\n");
        return nbCandidats;
    }
    printf("Département :\n> ");
    scanf("%s", c.departement);
    rechercheDept(tiut[posVille]->ldept, c.departement, &trouve);
    if (!trouve)
    {
        printf("Département non trouvé.\n");
        return nbCandidats;
    }
    c.decisionAdmission = 0;
    c.decisionCandidat = 0;
    cand.lChoix = listenouvChoix();
    cand.lChoix = insererChoix(cand.lChoix, c);
    cand.nbChoix = 1;
    cand.num = nbCandidats + 1;
    tetud[nbCandidats] = (Etudiant *)malloc(sizeof(Etudiant));
    printf("Vous êtes le candidat numéro %d.\n", nbCandidats + 1);
    *tetud[nbCandidats] = cand;
    nbCandidats++;
    return nbCandidats;
}