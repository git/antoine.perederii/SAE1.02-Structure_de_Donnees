var indexSectionsWithContent =
{
  0: "acdegilmnprstv",
  1: "cemv",
  2: "acmrs",
  3: "acdegilmnprstv",
  4: "cdlnprstv",
  5: "lm",
  6: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Pages"
};

