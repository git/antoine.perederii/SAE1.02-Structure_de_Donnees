/**
 * @file affichage.c
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier contenant les fonctions d'affichage
 * 
 */

#include "SAE.h"

/**
 * @brief Affiche les villes contenant un IUT
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void affichageVillesIUT(VilleIUT *tiut[], int nbIUT)
{
    printf("\nVoici les villes qui ont un IUT :\n\n");
    for (int i = 0; i < nbIUT; i++)
    {
        printf("\t%s\n", tiut[i]->ville);
    }
}

/**
 * @brief Affiche les départements de chaque IUT
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void affichageDeptIUT(VilleIUT *tiut[], int nbIUT)
{
    printf("\nVoici les départements présents dans chaque IUT :\n\n");
    for (int i = 0; i < nbIUT; i++)
    {
        printf("\t%s :\n", tiut[i]->ville);
        afficherDept(tiut[i]->ldept);
    }
}

/**
 * @brief Affiche le nombre de places dans chaque département
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void affichageNbP(VilleIUT *tiut[], int nbIUT)
{
    printf("\nVoici le nombre de places dans chaque département de chaque IUT :\n\n");
    for (int iut = 0; iut < nbIUT; iut++)
    {
        printf("\t%s :\n", tiut[iut]->ville);
        for (int dept = 0; dept < longueur(tiut[iut]->ldept); dept++)
        {
            printf("\t\t%s :\t%i\n", getDept(tiut[iut]->ldept, dept), getNbP(tiut[iut]->ldept, dept));
        }
    }
}

/**
 * @brief Affiche un département particulier
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @param nbIUT Nombre d'IUT
 */
void affichageDeptParticulier(VilleIUT *tiut[], int nbIUT)
{
    char deptRecherche[30];
    bool trouve, deptExiste = false;
    printf("\nQuel département souhaitez-vous rechercher ?\n> ");
    scanf("%s", deptRecherche);
    printf("\nVoici les villes possédant le département %s :\n\n", deptRecherche);
    for (int iut = 0; iut < nbIUT; iut++)
    {
        rechercheDept(tiut[iut]->ldept, deptRecherche, &trouve);
        if (trouve == true)
        {
            printf("\t%s\n", tiut[iut]->ville);
            deptExiste = true;
        }
    }
    if (deptExiste == false)
    {
        printf("\tAucun\n");
    }
}

/**
 * @brief Affiche les informations d'un candidat
 *
 * @author Antoine Perederii
 * @param mEtudiant Informations d'un candidat
 */
void afficherEtudiant(Etudiant mEtudiant)
{
    printf("\nNuméro : %d\nNom : %s\nPrénom : %s\nNotes : %.2f\t%.2f\t%.2f\t%.2f\t%.2f\nNombre de choix : %d\n", mEtudiant.num, mEtudiant.nom, mEtudiant.prenom, mEtudiant.tabNotes[0], mEtudiant.tabNotes[1], mEtudiant.tabNotes[2], mEtudiant.tabNotes[3], mEtudiant.tabNotes[4], mEtudiant.nbChoix);
    afficherChoix(mEtudiant.lChoix);
    printf("\n");
}

/**
 * @brief Affiche les informations d'un candidat particulier
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void afficherCandidat(Etudiant *tetud[], int nbCandidats)
{
    int num, index;
    bool trouve;
    printf("Veuillez entrer le numéro de l'étudiant recherché :\n> ");
    scanf("%d", &num);
    index = rechercheCandidat(tetud, nbCandidats, num, &trouve);
    if (trouve == false)
    {
        printf("Le numéro de l'étudiant recherché n'existe pas.\n");
        return;
    }
    afficherEtudiant(*tetud[index]);
}

/**
 * @brief Affiche les informations de tous les candidats
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void afficherCandidats(Etudiant *tetud[], int nbCandidats)
{
    if (nbCandidats == 0)
    {
        printf("Aucun candidat n'a été enregistré.\n");
        return;
    }
    for (int i = 0; i < nbCandidats; i++)
    {
        afficherEtudiant(*tetud[i]);
    }
}

/**
 * @brief Affiche les candidats admis
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats admis
 * @param nbCandidats Nombre de candidats admis
 */
void afficherCandidatsAdmis(Etudiant *tetud[], int nbCandidats)
{
    Etudiant mEtudiant;
    for (int i = 0; i < nbCandidats; i++)
    {
        mEtudiant = *tetud[i];
        if (tetud[i]->lChoix->choix.decisionAdmission == 1)
        {
            afficherEtudiant(*tetud[i]);
        }
    }
}

/**
 * @brief Affiche les candidats refusés
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void afficherCandidatsRefuses(Etudiant *tetud[], int nbCandidats)
{
    Etudiant mEtudiant;
    for (int i = 0; i < nbCandidats; i++)
    {
        mEtudiant = *tetud[i];
        if (mEtudiant.lChoix->choix.decisionAdmission == -1)
        {
            afficherEtudiant(mEtudiant);
        }
    }
}

/**
 * @brief Affiche les candidats sur liste d'attente
 *
 * @author Antoine Perederii
 * @param tetud Tableau des candidats
 * @param nbCandidats Nombre de candidats
 */
void afficherCandidatsAttente(Etudiant *tetud[], int nbCandidats)
{
    Etudiant mEtudiant;
    for (int i = 0; i < nbCandidats; i++)
    {
        mEtudiant = *tetud[i];
        if (tetud[i]->lChoix->choix.decisionAdmission == 2)
        {
            afficherEtudiant(*tetud[i]);
        }
    }
}
