/**
 * @file chargement.c
 * @author Kyllian Chabanon
 * @brief Fichier contenant les fonctions de chargement
 *
 */

#include "SAE.h"

/**
 * @brief Charge le fichier des IUT dans un tableau de pointeurs et retourne le nombre d'IUT
 *
 * @author Kyllian Chabanon
 * @param tiut Tableau des IUT
 * @return int
 */
int chargementVillesIUT(VilleIUT *tiut[])
{
    FILE *file = fopen("informationsIUT.txt", "r");
    char ville[30], dept[30], resp[30];
    int nbP, i = 0, insertPos;
    bool trouve;
    if (file == NULL)
    {
        printf("Fonction chargementVillesIUT : Problème lors de l'ouverture du fichier informationsIUT.txt\n");
        exit(-1);
    }

    while (!feof(file))
    {
        fscanf(file, "%s%s%d", ville, dept, &nbP);
        fseek(file, 1, SEEK_CUR);
        fgets(resp, 30, file);
        resp[strlen(resp) - 1] = '\0';
        insertPos = rechercheVille(tiut, i, ville, &trouve);

        if (trouve == 0)
        {
            tiut[i] = (VilleIUT *)malloc(sizeof(VilleIUT));
            if (tiut[i] == NULL)
            {
                printf("Fonction chargementVillesIUT : Problème lors du malloc\n");
                exit(-1);
            }
            strcpy(tiut[i]->ville, ville);

            tiut[i]->ldept = listenouv();
            tiut[i]->ldept = inserer(tiut[i]->ldept, dept, nbP, resp);
            i++;
        }
        else
        {
            rechercheDept(tiut[insertPos]->ldept, dept, &trouve);
            if (trouve == true)
            {
                break;
            }
            tiut[insertPos]->ldept = inserer(tiut[insertPos]->ldept, dept, nbP, resp);
        }
    }

    fclose(file);
    return i;
}

/**
 * @brief Charge le fichier des candidats dans un tableau de pointeurs et retourne le nombre de candidats
 *
 * @author Kyllian Chabanon
 * @param tetud Tableau des candidats
 * @return int
 */
int chargerCandidats(Etudiant *tetud[])
{
    FILE *file;
    file = fopen("candidats.txt", "r");
    int nbCandidats, i = 0;
    Choix choix;
    if (file == NULL)
    {
        printf("Erreur d'ouverture du fichier !\n");
        exit(1);
    }
    fscanf(file, "%d", &nbCandidats);
    while (i < nbCandidats)
    {
        tetud[i] = (Etudiant *)malloc(sizeof(Etudiant));
        if (tetud[i] == NULL)
        {
            printf("Erreur malloc\n");
            exit(2);
        }
        fscanf(file, "%d%s%s", &tetud[i]->num, tetud[i]->nom, tetud[i]->prenom);
        for (int note = 0; note < 4; note++)
        {
            fscanf(file, "%f", &tetud[i]->tabNotes[note]);
        }
        tetud[i]->tabNotes[4] = (tetud[i]->tabNotes[0] + tetud[i]->tabNotes[1] + tetud[i]->tabNotes[2] + tetud[i]->tabNotes[3]) / 4;
        fscanf(file, "%d", &tetud[i]->nbChoix);
        tetud[i]->lChoix = listenouvChoix();
        for (int n = 0; n < tetud[i]->nbChoix; n++)
        {
            fscanf(file, "%s%s%d%d", choix.ville, choix.departement, &choix.decisionAdmission, &choix.decisionCandidat);
            tetud[i]->lChoix = insererChoix(tetud[i]->lChoix, choix);
        }
        i++;
    }
    return i;
}
