/**
 * @file structures.h
 * @author Kyllian Chabanon - Antoine Perederii
 * @brief Fichier qui contient toutes les définitions de structures ainsi que les prototypes des fonctions relatives à celles-ci.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* Partie 1 */
/**
 * @brief Maillon d'une liste de départements
 *
 * @author Kyllian Chabanon
 */
typedef struct maillonDept
{
    char departement[30];     /**< Nom du département*/
    int nbP;                  /**< Nombre de places*/
    char resp[30];            /**< Nom du responsable du département*/
    struct maillonDept *suiv; /**< Pointeur vers le maillon suivant de la liste*/
} MaillonDept, *ListeDept;    /**< Pointeur sur une structure de type MaillonDept*/

/**
 * @brief Structure d'une ville possédant un IUT
 *
 * @author Kyllian Chabanon
 */
typedef struct
{
    char ville[30];  /**< Nom de la ville*/
    ListeDept ldept; /**< Liste des départements présents dans l'IUT de la ville*/
} VilleIUT;

/*  Partie 2    */
/**
 * @brief Structure d'un choix
 *
 * @author Antoine Perederii
 */
typedef struct
{
    char ville[30];        /**< Nom de la ville choisie*/
    char departement[30];  /**< Nom du département choisi*/
    int decisionAdmission; /**< Décision du département*/
    int decisionCandidat;  /**< Décision du candidat*/
} Choix;

/**
 * @brief Maillon d'une liste de choix
 *
 * @author Antoine Perederii
 */
typedef struct maillonChoix
{
    Choix choix;               /**< Choix du candidat*/
    struct maillonChoix *suiv; /**< Pointeur vers le maillon suivant de la liste*/
} MaillonChoix, *ListeChoix;   /**< Pointeur sur une structure de type MaillonChoix*/

/**
 * @brief Structure d'un candidat
 *
 * @author Antoine Perederii
 */
typedef struct
{
    int num;           /**< Numéro du candidat*/
    char nom[22];      /**< Nom du candidat*/
    char prenom[22];   /**< Prénom du candidat*/
    float tabNotes[5]; /**< Tableau des moyennes en mathématiques, français, anglais et en spécialité ainsi que sa moyenne générale*/
    int nbChoix;       /**< Nombre de choix formulés*/
    ListeChoix lChoix; /**< Liste des choix formulés*/
} Etudiant;

enum
{
    maths,
    francais,
    anglais,
    spe,
    moy
};

/* ListeDept */
ListeDept listenouv(void);
ListeDept insererEnTete(ListeDept ld, char departement[], int nbP, char resp[]);
ListeDept inserer(ListeDept ld, char departement[], int nbP, char resp[]);
void afficher(ListeDept ld);
bool vide(ListeDept ld);
void afficherDept(ListeDept ld);
int rechercheDept(ListeDept ld, char departement[], bool *trouve);
ListeDept supprimerEnTete(ListeDept ld);
ListeDept supprimer(ListeDept ld, char departement[]);
int longueur(ListeDept ld);
int getNbP(ListeDept ld, int pos);
void setNbP(ListeDept ld, int pos, int valeur);
char *getResp(ListeDept ld, int pos);
void setResp(ListeDept ld, int pos, char valeur[]);
char *getDept(ListeDept ld, int pos);
void setResp(ListeDept ld, int pos, char valeur[]);

/* ListeChoix */
ListeChoix listenouvChoix(void);
ListeChoix insererEnTeteChoix(ListeChoix lc, Choix choix);
ListeChoix insererChoix(ListeChoix lc, Choix choix);
ListeChoix supprimerEnTeteChoix(ListeChoix lc);
ListeChoix supprimerChoix(ListeChoix lc, Choix choix);
bool rechercheChoix(ListeChoix lc, Choix choix);
Choix TeteChoix(ListeChoix lc);
bool videChoix(ListeChoix lc);
void afficherChoix(ListeChoix lc);
int longueurChoix(ListeChoix lc);
void afficherCandidatsChoix(Choix choix);
char *getDeptChoix(ListeChoix lc, int pos);
char *getVilleChoix(ListeChoix lc, int pos);
int trouverPos(ListeChoix lc, char ville[], char dept[]);
void setDecisionAdmission(ListeChoix lc, int pos, int val);