#include "SAE.h"

void global(void)
{
    int nbVilles, nbCandidats, nbAdmis = 0, nbAttente = 0;
    bool phaseCandidatures = false, phaseTraitement = false;
    VilleIUT *tiut[100];
    Etudiant *tetud[100], *admis[100], *attente[100];
    nbVilles = chargementVillesIUT(tiut);
    nbCandidats = chargerCandidats(tetud);
    menu(tiut, &nbVilles, tetud, &nbCandidats, &phaseCandidatures, &phaseTraitement, admis, &nbAdmis, attente, &nbAttente);
    sauvegardeVillesIUT(tiut, nbVilles);
    sauvegardeAdmis(admis, nbAdmis);
    sauvegardeAttente(attente, nbAttente);
    sauvegardeTousCandidats(tetud, nbCandidats);
    libererEspaceCandidats(tetud, nbCandidats);
    libererEspaceIUT(tiut, nbVilles);
}
