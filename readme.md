# SAE S1.02 - Comparaison d'approches algorithmiques

> Compte rendu

## Partie 1

### 1 - Fonctionnalités

- Nous avons fait un menu qui nous permet d'accéder aux sous-menus administrateur et utilisateur.
- Un administrateur peut modifier le nombre de places dans un département, créer et supprimer un département dans un IUT, modifier le responsable d'un département, afficher les informations d'un ou de tous les candidats et lancer ainsi que stopper la phase de candidatures.
- Un utilisateur peut voir les villes possédant un IUT, voir les départements dans chaque IUT, voir le nombre de places disponibles en première année par département et voir les IUT possédant un département en particulier.
- Lorsqu'un administrateur supprime le dernier département d'un IUT, cet IUT est aussi supprimé.
- Lorsqu'un administrateur essaie de créer un département dans un IUT inexistant, il lui est proposé de le créer en même temps.

### 1 - Structures de fichiers

Nous avons un fichier texte `informationsIUT.txt` qui contient les informations sur chaque IUT et département. Les données sont stockées sous la forme suivante et sont séparées par un espace :  
_ville de l'IUT_, _nom du département_, _nombre de places en première année_ et _nom du responsable_.  
Nous avons choisi de faire un fichier texte car il ne contient aucune donnée sensible, il n'est donc pas nécessaire de le convertir en binaire.

### 1 - Structures de données

Nous avons une structure `VilleIUT` qui contient le nom d'une ville ainsi qu'une liste chaînée de type `ListeDept` contenant tous les départements dans cette ville. La liste `ListeDept` est un pointeur vers un maillon `MaillonDept` qui contient le nom du département, le nombre de places en première année, le nom du responsable et un pointeur vers le maillon suivant de la liste.  
Nous avons décidé de stocker les différents IUT via un tableau de pointeurs sur la structure `VilleIUT`.

## Partie 2

### 2 - Fonctionnalités

- Nous avons ajouté un système de candidats. Les utilisateurs peuvent devenir candidats et postuler dans des IUT.
- Lors de l'inscription en tant que candidat, il est demandé au nouvel inscrit de formuler un choix. Il pourra en rajouter ou en supprimer ultérieurement.
- Un candidat peut afficher ses informations et ajouter, supprimer ou modifier un choix.

### 2 - Structures de fichiers

Les données sur les candidats sont stockées dans un fichier texte `candidats.txt`. Le nombre total de candidats est écrit sur la première ligne du fichier. Les données sont stockées sous la forme :  
_numéro du candidat_, _nom_, _prénom_, _liste des notes_, _nombre de choix_ et pour chaque choix : _ville choisie_, _département_, _décision du département_, _décision du candidat_.  
Chaque donnée est séparée par un retour à la ligne, à part les différentes notes de la liste des notes qui sont séparées par un espace.  
La décision du département peut prendre les valeurs _0_ si le dossier du candidat n'a pas encore été traité, _1_ si le candidat a été accepté, _2_ s'il est sur liste d'attente et _-1_ s'il a été refusé.  
La décision du candidat peut prendre les valeurs _0_ s'il n'a pas encore décidé, _1_ s'il a accepté la décision du département et _-1_ s'il l'a refusé.

### 2 - Structures de données

Nous avons une structure `Etudiant` qui contient le numéro d'un candidat, son nom, son prénom, un tableau de taille physique 5 qui contient ses 4 moyennes dans les différentes matières ainsi que sa moyenne générale, son nombre de choix et la liste de ses choix qui est de type `ListeChoix`.  
La structure `ListeChoix` est un pointeur sur la structure `MaillonChoix`, composé du choix de la structure `Choix` et d'un pointeur sur l'élément suivant de la liste.  
La structure `Choix` contient la ville du choix, le déparement, la décision du département et la décision du candidat.  
Ces données sont chargées sous la forme d'un tableau de pointeurs sur la structure `Etudiant`.

## Partie 3

### 3 - Fonctionnalités

- la partie 3 permet de gérer les candidatures. Lorsque l'administrateur lance la phase d'admission, le responsable de chaque département peut consulter les candidatures dans son département et les accepter ou les refuser.
- Pour cela, le responsable va choisir la moyenne minimale à partir de laquelle les gens sont acceptés ainsi que le nombre de places disponibles.
- Le tableau des candidats de son département est trié par ordre décroissant de la moyenne des 4 matières du candidat. Lorsque le candidat a une moyenne supérieure à la moyenne minimale et qu'il reste des places, il est accepté d'office. S'il a une moyenne supérieure à la moyenne minimale mais qu'il n'y a plus de places dans le département, il est mis en liste d'attente. S'il a une moyenne inférieure à la moyenne minimale, il est refusé.

### 3 - Structure des fichiers
Les données des candidats admis sont sauvegardées dans un fichier `candidatsAdmis.txt` et celles des candidats en liste d'attente sont sauvegardées dans le fichier `candidatsAttente.txt`.

### 3 - Structures de données
Les candidats admis et en liste d'attente sont mis dans des tableaux spécifiques.